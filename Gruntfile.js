'use strict';

module.exports = function(grunt){
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		phpunit: {
			test: {
				dir: '',
				options: {
					bin: 'vendor/bin/phpunit',
					configuration: 'test/phpunit.xml',
				}
				
			},
			coverage: {
				dir: '',
				options: {
					bin: 'vendor/bin/phpunit',
					configuration: 'test/phpunit.xml',
					coverageHtml: "coverage"
				}
				
			}
		},
	});
	
	grunt.loadNpmTasks('grunt-phpunit');
	
	grunt.registerTask( 'test', ['phpunit:test'] );
	grunt.registerTask( 'coverage', ['phpunit:coverage'] );
	
	
};