<?php

namespace SellShed\PushNotification\DAO;

/**
 * the Base DAO
 * @author SellShed
 *
 */
class BaseDAO{

	/**
	 * the driver controller
	 */
	protected $drivers;
	
	/**
	 * the constructor that populates the drivers
	 * @param \SellShed\Driver\DriverController $drivers
	 */
	protected function __construct($drivers){
		$this->drivers = $drivers;
	}
	
}