<?php

namespace SellShed\PushNotification\DAO;

/**
 * the Base DAO for any DAO's that use database access
 * @author SellShed
 *
 */
abstract class BaseDatabaseDAO extends BaseDAO{
	
	/**
	 * must be overridden to point to the table name
	 */
	protected $_table = false;
	
	/**
	 * must be overridden to point to the primary key of the table.
	 * This is always an int value
	 */
	protected $_primary = false;
	
	/**
	 * must be overridden to point to an optional unique id of the table
	 */
	protected $_key = false;
	
	/**
	 * A list of column names
	 */
	protected $_columns = false;
	
	/**
	 * a mapping of json to column names
	 */
	protected $_mappings = false;
	
	/**
	 * the reverse mapping which is automatically generated from the mappings
	 */
	protected $_reverse_mappings;
	
	/**
	 * the constructor that populates the drivers
	 * @param \SellShed\Driver\DriverController $drivers
	 */
	protected function __construct($drivers){
		 parent::__construct($drivers);
		 
	}

	/**
	 * returns the mappings as a reverse map
	 */
	private function _getReverseMap(){
		if (!isset($this->_reverse_mappings)){
			$this->_reverse_mappings = array_flip($this->_mappings);
		}
		
		return $this->_reverse_mappings;
	}
	
	
	/**
	 * maps data to a json object
	 */
	public function mapToJson($db_row_data){
		
		$r_map = $this->_getReverseMap();		
		$toreturn = array();
		
		foreach ($db_row_data as $name => $value){
			if (isset($r_map[$name])){
				$toreturn[$r_map[$name]] = $value;
			}
		}
		
		return $toreturn;
		
	}
	
	
	/**
	 * gets all columns from the primary key
	 * @param int $id
	 */
// CURRENTLY UNSED SO COMMENTING OUT
// 	public function getFromPrimary($id){
// 		$db = $this->drivers->getDatabase();
// 		$data = $db->getRow($this->_table,$this->_primary,$id, \PDO::PARAM_INT);

// 		if ($data!=false){
// 			return $this->mapToJson($data);
// 		}
// 		return false;
		
// 	}
	
	/**
	 * gets all columns from the key
	 * @param unknown $key the key value
	 */
	public function getFromKey($key){
		$db = $this->drivers->getDatabase();
		$data =  $db->getRow($this->_table,$this->_key,$key, $this->_columns[$this->_key]);
		
		if ($data!=false){
			return $this->mapToJson($data);
		}
		return false;
	}
	
	/**
	 * Removes a watchlist record
	 * @param string $key
	 */
	public function deleteFromKey($key){
		
		$db = $this->drivers->getDatabase();
		return $db->removeRow($this->_table,$this->_key,$key, $this->_columns[$this->_key]);
		
	}
	
// 	/**
// 	 * dump table to output
// 	 */
// 	public function dumpTable(){
// 		$db = $this->drivers->getDatabase();
		
// 		$conn = $db->getDatabaseConnection();
		
// 		$stmt = $conn->prepare('SELECT * FROM `'.$this->_table.'`');
		
// 			if (!$stmt->execute()){
// 			return false;
// 		}
		
// 		// fetch row
// 		var_dump( $stmt->fetchAll());
		
		
// 	}
	
}