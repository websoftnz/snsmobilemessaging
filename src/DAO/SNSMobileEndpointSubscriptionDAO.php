<?php
namespace SellShed\PushNotification\DAO;

/**
 * SNS Topic Subscription Data Access Object
 * @author SellShed
 */
class SNSMobileEndpointSubscriptionDAO extends BaseDatabaseDAO{
	
	/**
	 * the only instance of this access Object
	 */
	private static $instance;
	
	protected $_primary = "sns_topic_mobile_subscription_id";
	
	protected $_table = "sns_topic_mobile_subscription";
	
	protected $_columns = array(
			"sns_topic_mobile_subscription_id" => \PDO::PARAM_INT,
			"fk_sns_mobile_endpoint_id" => \PDO::PARAM_INT,
			"fk_sns_topic_id" => \PDO::PARAM_INT,
			"topic_subscription_arn"=> \PDO::PARAM_STR,
			
	);
	
	protected $_mappings = array(
			"sns_topic_mobile_subscription_id" => "sns_topic_mobile_subscription_id",
			"fk_sns_mobile_endpoint_id" => "fk_sns_mobile_endpoint_id",
			"fk_sns_topic_id" => "fk_sns_topic_id",
			"topic_subscription_arn"=>"topic_subscription_arn"
	);
	
	
	/**
	 * returns an instance of this access object
	 * @param \SellShed\Driver\DriverController $drivers the driver controller
	*/
	public static function instance($drivers){
		if (!isset(self::$instance)){
			self::$instance = new SNSMobileEndpointSubscriptionDAO($drivers);
		}
		return self::$instance ;
	}
	
	/**
	 * returns a list of user subscribed topics
	 * @param int $userid the user identifier
	 */
	public function getAllMobileEndpointSubscriptions($endpointId){
		
		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
		
		$stmt= $conn->prepare("SELECT * FROM sns_topic_mobile_subscription WHERE fk_sns_mobile_endpoint_id=:endpointId");
		$stmt->bindParam(':endpointId', $endpointId, $this->_columns['sns_topic_mobile_subscription_id']);
		
		if (!$stmt->execute()){
			return false;
		}
		
		$allFetchData = $stmt->fetchAll(); 

		return $allFetchData;
	}
	
	/**
	 * returns a list of mobile endpoint subscriptions for a user
	 * @param int $userId the user id
	 */
	public function getAllMobileEndpointSubscriptionsFromUser($userId){
		
		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
		
		$stmt= $conn->prepare("SELECT * FROM sns_topic_mobile_subscription INNER JOIN `sns_mobile_endpoint`".
				" ON `sns_mobile_endpoint`.`sns_mobile_endpoint_id` = `".$this->_table."`.`fk_sns_mobile_endpoint_id`".
				" WHERE `sns_mobile_endpoint`.`fk_user_id` = :userid");
		
		$stmt->bindParam(':userid', $userId, \PDO::PARAM_INT);

		if (!$stmt->execute()){
			return false;
		}
		
		$allFetchData = $stmt->fetchAll();
		
		return $allFetchData;
	}
	
	/**
	 * retuns a list of mobile endpoint subscriptions for a user for the given topic
	 * @param int $topicId the topic identifier
	 * @param int $userId the user id
	 */
	public function getAllMobileEndpointSubscriptionsForUserAndTopic($userId, $topicId){
		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
		
		$stmt= $conn->prepare("SELECT * FROM sns_topic_mobile_subscription INNER JOIN `sns_mobile_endpoint`".
				" ON `sns_mobile_endpoint`.`sns_mobile_endpoint_id` = `".$this->_table."`.`fk_sns_mobile_endpoint_id`".
				" WHERE `sns_mobile_endpoint`.`fk_user_id` = :userid AND `fk_sns_topic_id` = :topicid ");
		
		$stmt->bindParam(':userid', $userId, \PDO::PARAM_INT);
		$stmt->bindParam(':topicid', $topicId, $this->_columns['fk_sns_topic_id']);
		
		if (!$stmt->execute()){
			return false;
		}
		
		$allFetchData = $stmt->fetchAll();
		
		return $allFetchData;
		
	}
	

	/**
	 * clears all subscribed topics for a user and topic combination
	 * @param int $userid the user identifier
	 * @param int $topicId the topic identifier
	 */
	public function removeFromTopicAndUser($userId, $topicId){
	
		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
	
		$stmt = $conn->prepare('DELETE FROM `'.$this->_table.
				"` WHERE `fk_sns_topic_id` = :topicid AND fk_sns_mobile_endpoint_id IN( ".
				" SELECT sns_mobile_endpoint_id FROM `sns_mobile_endpoint`".
				" WHERE `sns_mobile_endpoint`.`fk_user_id` = :userid)");
		
		$stmt->bindParam(':userid', $userId, \PDO::PARAM_INT);
		$stmt->bindParam(':topicid', $topicId, $this->_columns['fk_sns_topic_id']);
		
		if (!$stmt->execute()){
			return false;
		} else {
			return true;
		}
	
	}
	
	
	/**
	 * adds an topic subscription 
	 * @param int $topicId the topic id
	 * @param int $mobileEndpointId the mobile endpoint id
	 * @param string $arn the endpoint arn
	 */
	public function addMobileEndpoint($topicId, $mobileEndpointId, $arn){
		$db = $this->drivers->getDatabase();
	
		$db->insertRow($this->_table, 	array(
					array("col"=>"fk_sns_mobile_endpoint_id", "type" => $this->_columns["fk_sns_mobile_endpoint_id"], "value" => $mobileEndpointId),
					array("col"=>"fk_sns_topic_id", "type" => $this->_columns["fk_sns_topic_id"], "value" =>$topicId),
					array("col"=>"topic_subscription_arn", "type" => $this->_columns["topic_subscription_arn"], "value" =>$arn)
				));
		
		
		return true;
	}
	
	/**
	 * updates the arn for a given topic subscription
	 */
	public function updateSubscriptionArn($subscriptionId, $arn){
		$db = $this->drivers->getDatabase();
		
		$data = $db->updateRow($this->_table,
				$this->_primary,$subscriptionId,$this->_columns[$this->_primary],
				array(
						array("col"=>"topic_subscription_arn", "type" => $this->_columns["topic_subscription_arn"], "value" =>$arn)
				));
		
		return true;
	}

// 	/**
// 	 * removes a topic subscription
// 	 * @param int $arn the arn
// 	 */
// 	public function removeFromArn($arn){
// 		$db = $this->drivers->getDatabase();
	
// 		$conn = $db->getDatabaseConnection();
		
// 		$stmt = $conn->prepare('DELETE FROM `'.$table.'` WHERE `topic_subscription_arn` = :arn');
// 		$stmt->bindParam(':arn', $userId,  $this->_columns["topic_subscription_arn"]);
		
// 		if (!$stmt->execute()){
// 			return false;
// 		} else {
// 			return true;
// 		}
// 	}
	
	
// 	/**
// 	 * removes all topic subscriptions for the given topic
// 	 * @param string $topic the arn
// 	 */
// 	public function removeFromTopicId($topicId){
// 		$db = $this->drivers->getDatabase();
	
// 		$conn = $db->getDatabaseConnection();
	
// 		$stmt = $conn->prepare('DELETE FROM `'.$table.'` WHERE `fk_sns_topic_id` = :topicId');
// 		$stmt->bindParam(':topicId', $topicId,  $this->_columns["fk_sns_topic_id"]);
	
// 		if (!$stmt->execute()){
// 			return false;
// 		} else {
// 			return true;
// 		}
// 	}
	
	/**
	 * clear all subs for a device
	 * @param int $mobileEndpointId the id of the mobile endpoint
	 */
	public function removeFromMobileEndpoint($mobileEndpointId){
		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
		
		$stmt = $conn->prepare('DELETE FROM `'.$this->_table.
				"` WHERE `fk_sns_mobile_endpoint_id` = :endpointId");
		$stmt->bindParam(':endpointId', $mobileEndpointId, $this->_columns['fk_sns_mobile_endpoint_id']);
		
		if (!$stmt->execute()){
			return false;
		} else {
			return true;
		}
		
	}
	
	
	/**
	 * clears all subscribed topics
	 * @param int $userid the user identifier
	 */
	public function removeAllUserSubscriptions($userId){
	
		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
	
		$stmt = $conn->prepare("DELETE FROM `".$this->_table.
				"` WHERE fk_sns_mobile_endpoint_id in (SELECT sns_mobile_endpoint_id FROM `sns_mobile_endpoint`".
				" WHERE `sns_mobile_endpoint`.`fk_user_id` = :userid )");
		$stmt->bindParam(':userid', $userId, \PDO::PARAM_INT);
	
		if (!$stmt->execute()){
			return false;
		} else {
			return true;
		}
	
	}
	
	/**
	 * gets a subscription from the given details
	 * 
	 */
	public function getSubscriptionArn($topicid, 
						$snsMobileEndpointId){
		
		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
		
		$stmt= $conn->prepare("SELECT `sns_topic_mobile_subscription_id`, `topic_subscription_arn` FROM sns_topic_mobile_subscription WHERE fk_sns_topic_id=:topicid and fk_sns_mobile_endpoint_id=:snsmobileendpoint");
		$stmt->bindParam(':topicid', $topicid, $this->_columns['fk_sns_topic_id']);
		$stmt->bindParam(':snsmobileendpoint', $snsMobileEndpointId, $this->_columns['fk_sns_mobile_endpoint_id']);
		
		if (!$stmt->execute()){
			return false;
		}
		
		$fetchData = $stmt->fetch();
		
		return $fetchData;
		
	}
	
}