<?php
namespace SellShed\PushNotification\DAO;

/**
 * SNS Topic Subscription Data Access Object
 * @author SellShed
 */
class SNSTopicSubscriptionDAO extends BaseDatabaseDAO{
	
	/**
	 * the only instance of this access Object
	 */
	private static $instance;
	
	protected $_primary = "sns_topic_subscription_id";
	
	protected $_table = "sns_topic_subscription";
	
	protected $_columns = array(
			"sns_topic_subscription_id" => \PDO::PARAM_INT,
			"fk_user_id" => \PDO::PARAM_INT,
			"fk_sns_topic_id" => \PDO::PARAM_INT
			
	);
	
	protected $_mappings = array(
			"sns_topic_subscription_id" => "sns_topic_subscription_id",
			"fk_user_id" => "fk_user_id",
			"fk_sns_topic_id" => "fk_sns_topic_id",
	);
	
	
	/**
	 * returns an instance of this access object
	 * @param \SellShed\Driver\DriverController $drivers the driver controller
	*/
	public static function instance($drivers){
		if (!isset(self::$instance)){
			self::$instance = new SNSTopicSubscriptionDAO($drivers);
		}
		return self::$instance ;
	}
	
	/**
	 * returns a list of user subscribed topics
	 * @param int $userid the user identifier
	 */
	public function getTopicSubscriptions($userId){
		
		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
		
		$stmt= $conn->prepare("SELECT * FROM sns_topic_subscription JOIN sns_topic on `sns_topic`.`sns_topic_id`=`sns_topic_subscription`.`fk_sns_topic_id` WHERE `fk_user_id`=:userid");
		$stmt->bindParam(':userid', $userId, $this->_columns['fk_user_id']);
		
		if (!$stmt->execute()){
			return false;
		}
		
		$allFetchData = $stmt->fetchAll(); 

		return $allFetchData;
	}
	
	/**
	 * clears all subscribed topics
	 * @param int $userid the user identifier
	 */
	public function removeAllUserSubscriptions($userId){

		$db = $this->drivers->getDatabase();
		$conn = $db->getDatabaseConnection();
		
		$stmt = $conn->prepare('DELETE FROM `'.$this->_table.'` WHERE `fk_user_id` = :userid');
		$stmt->bindParam(':userid', $userId, $this->_columns['fk_user_id']);
		
		if (!$stmt->execute()){
			return false;
		} else {
			return true;
		}
		
	}
	
	/**
	 * adds a user to a topic
	 * @param int $topicId the topic id
	 * @param int $userId the user id
	 */
	public function subscribeUserToTopic($topicId, $userId){
		$db = $this->drivers->getDatabase();
	
		$db->insertRow($this->_table, 	array(
					array("col"=>"fk_user_id", "type" => $this->_columns["fk_user_id"], "value" => $userId),
					array("col"=>"fk_sns_topic_id", "type" => $this->_columns["fk_sns_topic_id"], "value" =>$topicId)));
		
		return true;
	}

	/**
	 * checks whether a user subscription exists already
	 * @param int $topicId the topic id
	 * @param int $userId the user id
	 */
	public function checkUserSubscriptions($topicId, $userId){
		$db = $this->drivers->getDatabase();

		$conn = $db->getDatabaseConnection();
		
		
		$stmt= $conn->prepare("SELECT sns_topic_subscription_id FROM sns_topic_subscription WHERE `fk_user_id` = :userid and `fk_sns_topic_id` = :topicid");
		$stmt->bindParam(':userid', $userId,  $this->_columns["fk_user_id"]);
		$stmt->bindParam(':topicid', $topicId,  $this->_columns["fk_sns_topic_id"]);
		
		if (!$stmt->execute()){
			return false;
		}
		
		$allFetchData = $stmt->fetchAll();
		
		return count($allFetchData)>0;
		
		
	}
	
	/**
	 * removes a user from a topic
	 * @param int $topicId the topic id
	 * @param int $userId the user id
	 */
	public function unsubscribeUserFromTopic($topicId, $userId){
		$db = $this->drivers->getDatabase();
	
		$conn = $db->getDatabaseConnection();
		
		$stmt = $conn->prepare('DELETE FROM `'.$this->_table.'` WHERE `fk_user_id` = :userid and `fk_sns_topic_id` = :topicid');
		$stmt->bindParam(':userid', $userId,  $this->_columns["fk_user_id"]);
		$stmt->bindParam(':topicid', $topicId,  $this->_columns["fk_sns_topic_id"]);
		
		if (!$stmt->execute()){
			return false;
		} else {
			return true;
		}
	}
}