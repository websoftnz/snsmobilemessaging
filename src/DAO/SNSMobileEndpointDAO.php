<?php
namespace SellShed\PushNotification\DAO;

/**
 * SNS Mobile Endpoint Data Access Object
 * @author SellShed
 */
class SNSMobileEndpointDAO extends BaseDatabaseDAO{

	/**
	 * the only instance of this access Object
	 */
	private static $instance;
	
	protected $_primary = "sns_mobile_endpoint_id";
	
	protected $_key = "sns_mobile_endpoint_push_token";
	
	protected $_table = "sns_mobile_endpoint";
	
	protected $_columns = array(
			"sns_mobile_endpoint_id" => \PDO::PARAM_INT,
			"sns_mobile_endpoint_push_token" => \PDO::PARAM_STR,
			"fk_user_id" => \PDO::PARAM_INT,
			"sns_mobile_endpoint_endpoint_arn" => \PDO::PARAM_STR,
			"sns_mobile_endpoint_device_type" => \PDO::PARAM_INT,
			//"sns_mobile_endpoint_device_token" => \PDO::PARAM_STR,
			"sns_mobile_endpoint_created_date" => \PDO::PARAM_STR,
			"sns_mobile_endpoint_lastupdated_date" => \PDO::PARAM_STR,
	);
	
	protected $_mappings = array(
			"sns_mobile_endpoint_id" => "sns_mobile_endpoint_id",
			"sns_mobile_endpoint_push_token" => "sns_mobile_endpoint_push_token",
			"fk_user_id" => "fk_user_id",
			"sns_mobile_endpoint_device_type" => "sns_mobile_endpoint_device_type",
			"sns_mobile_endpoint_endpoint_arn" => "sns_mobile_endpoint_endpoint_arn",
			// "sns_mobile_endpoint_device_token" => "sns_mobile_endpoint_device_token",
			"sns_mobile_endpoint_created_date" => "sns_mobile_endpoint_created_date",
			"sns_mobile_endpoint_lastupdated_date" => "sns_mobile_endpoint_lastupdated_date"
	);
	

	/**
	 * returns an instance of this access object
	 * @param \SellShed\Driver\DriverController $drivers the driver controller
	 */
	public static function instance($drivers){
		if (!isset(self::$instance)){
			self::$instance = new SNSMobileEndpointDAO($drivers);
		}
		return self::$instance ;
	}
	
	/**
	 * public function to add a new endpoint
	 */
	public function addEndpoint($pushToken, $deviceType, $userid, $endpointArn){
		$db = $this->drivers->getDatabase();
		
		return $db->insertRow($this->_table, array(
				array("col"=>"sns_mobile_endpoint_push_token", "type" => $this->_columns["sns_mobile_endpoint_push_token"], "value" => $pushToken),
				array("col"=>"sns_mobile_endpoint_device_type", "type" => $this->_columns["sns_mobile_endpoint_device_type"], "value" => $deviceType),
				array("col"=>"fk_user_id", "type" => $this->_columns["fk_user_id"], "value" => $userid),
				array("col"=>"sns_mobile_endpoint_endpoint_arn", "type" => $this->_columns["sns_mobile_endpoint_endpoint_arn"], "value" => $endpointArn),
		));
		
	}
	
	/**
	 * method to link the given user id to the given mobile endpoint
	 * @param string $pushToken the pushtoken
	 * @param int $userId the user identifier
	 */
	public function setUser($pushToken, $userId){
		$db = $this->drivers->getDatabase();
		
		$data = $db->updateRow($this->_table,
				$this->_key,$pushToken,$this->_columns[$this->_key],
				array(
						array("col"=>"fk_user_id", "type" => $this->_columns["fk_user_id"], "value" =>$userId)
				));
		
		return true;
	}
	
	/**
	 * updated the ARN for the given user device
	 * @param int $rowId 
	 * @param string $newArn
	 */
	public function updateArn($rowId, $newArn){
		$db = $this->drivers->getDatabase();
		
		$data = $db->updateRow($this->_table,
				$this->_primary,$rowId,$this->_columns[$this->_primary],
				array(
						array("col"=>"sns_mobile_endpoint_endpoint_arn", "type" => $this->_columns["sns_mobile_endpoint_endpoint_arn"], "value" =>$newArn)
				));
		
		return true;
		
	}
	
	/**
	 * removes a user from all relevant mobile endpoints
	 * @param int $userId the user id to remove
	 */
	public function removeUser($userId){
		$db = $this->drivers->getDatabase();
		
		$data = $db->updateRow($this->_table,
				"fk_user_id",$userId,$this->_columns["fk_user_id"],
				array(
						array("col"=>"fk_user_id", "type" => $this->_columns["fk_user_id"], "value" =>0)
				));
		
		return $data;
	}
	
	public function getUserEndpoints($userId){
		$db = $this->drivers->getDatabase();

		$conn = $db->getDatabaseConnection();
		
		$stmt = $conn->prepare('SELECT sns_mobile_endpoint_endpoint_arn, sns_mobile_endpoint_id FROM `'.$this->_table.'` WHERE `fk_user_id` = :id');
		$stmt->bindParam(':id', $userId, $this->_columns["fk_user_id"]);
		
		
		if ($stmt->execute()){
			return $stmt->fetchAll();
		}
		
		return false;
				
	}
	
	/**
	 * returns the endpoint arn
	 * ** currently Unused method
	 */
// 	public function getEndpointArn($pushToken){
// 		$db = $this->drivers->getDatabase();
		
// 		$conn = $db->getDatabaseConnection();
		
// 		$stmt = $conn->prepare('SELECT sns_mobile_endpoint_endpoint_arn FROM `'.$this->_table.'` WHERE `'.$this->_key.'` = :id');
// 		$stmt->bindParam(':id', $pushToken, $this->_columns[$this->_key]);
		

// 		if ($stmt->execute()){
// 			$data = $stmt->fetch();
			
// 			if (isset($data['sns_mobile_endpoint_endpoint_arn'])){
// 				return $data['sns_mobile_endpoint_endpoint_arn'];
				
// 			}
// 		}
// 		return false;
		
// 	}
	
}