<?php
namespace SellShed\PushNotification\DAO;

/**
 * SNS Topic Data Access Object
 * @author SellShed
 */
class SNSTopicDAO extends BaseDatabaseDAO{
	
	/**
	 * the only instance of this access Object
	 */
	private static $instance;
	
	protected $_primary = "sns_topic_id";
	
	protected $_key = "sns_topic_name";
	
	protected $_table = "sns_topic";
	
	protected $_columns = array(
			"sns_topic_id" => \PDO::PARAM_INT,
			"sns_topic_name" => \PDO::PARAM_STR,
			"sns_topic_arn" => \PDO::PARAM_STR,
			"sns_topic_created_date" => \PDO::PARAM_STR,
			"sns_topic_lastupdated_date" => \PDO::PARAM_STR,
	);
	
	protected $_mappings = array(
			"sns_topic_id" => "sns_topic_id",
			"sns_topic_name" => "sns_topic_name",
			"sns_topic_arn" => "sns_topic_arn",
			"sns_topic_created_date" => "sns_topic_created_date",
			"sns_topic_lastupdated_date" => "sns_topic_lastupdated_date"
	);
	
	
	/**
	 * returns an instance of this access object
	 * @param \SellShed\Driver\DriverController $drivers the driver controller
	*/
	public static function instance($drivers){
		if (!isset(self::$instance)){
			self::$instance = new SNSTopicDAO($drivers);
		}
		return self::$instance ;
	}
	
	/**
	 * convert the topic name 
	 * @param string $name the name
	 */
	private function convertTopicName($name){
		return trim(strtolower($name));
	}
	
	/**
	 * get a topic
	 * @param string $topicName the topic name
	 */
	public function getTopic($topicName){
		// convert to lower and remove spaces
		$name = $this->convertTopicName($topicName);
		return $this->getFromKey($name);
	}
	
	/**
	 * Create a new topic for the given name
	 * @param string $topicname the topic name
	 */
	public function createTopic($topicName){
		$name = $this->convertTopicName($topicName);
		
		$db = $this->drivers->getDatabase();
		$now =  date('Y-m-d H:i:s');
		$id = $db->insertRow($this->_table, 	array(
				array("col"=>"sns_topic_name", "type" => $this->_columns["sns_topic_name"], "value" => $name),
				array("col"=>"sns_topic_created_date", "type" => $this->_columns["sns_topic_created_date"], "value" =>$now),
				array("col"=>"sns_topic_lastupdated_date", "type" => $this->_columns["sns_topic_lastupdated_date"], "value" =>$now),
		));
		
		return $id;
	}
	
	/**
	 * link ARN to topic
	 */
	public function linkARNtoTopic($topicId, $topicArn){
		$db = $this->drivers->getDatabase();
		
		$data = $db->updateRow($this->_table,
				$this->_primary,$topicId,$this->_columns[$this->_primary],
				array(
						array("col"=>"sns_topic_arn", "type" => $this->_columns["sns_topic_arn"], "value" =>$topicArn)
				));
		
		return true;
		
	}
}