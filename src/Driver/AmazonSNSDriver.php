<?php

namespace SellShed\PushNotification\Driver;

use SellShed\PushNotification\PushNotification;
use Aws\Sns\Exception\SnsException;

/**
 * the database driver 
 * @author SellShed
 *
 */
class AmazonSNSDriver{
	
	private $platformArns = array();
	
	
	/**
	 * Constructs a database driver using the given config
	 * @param Array $config the config to use
	 */
	
	public function __construct($config){
		$this->config = $config;
		
		$this->platformArns[PushNotification::DEVICE_TYPE_ANDROID] = $config['PLATFORM_ARN_ANDROID'];
		$this->platformArns[PushNotification::DEVICE_TYPE_IOS] = $config['PLATFORM_ARN_IOS'];
		
	}
	

	
	/**
	 * creates a SNS connection
	 */
	private function _getSNSConnection(){
	
		if (!isset($this->snsConnection)){
			if (isset($this->config["region"])){
	
				
				$credentials = new \Aws\Credentials\Credentials($this->config["key"], $this->config["secret"]);
	
				$this->snsConnection = new \Aws\Sns\SnsClient([
						'credentials' => $credentials,
						'version' => 'latest',
						'region'  => $this->config['region'],
						'http'    => [
								'verify' => false
						]
				]);
				
			}
			else
			{
				$this->snsConnection = null;
			}
		}
	
		return $this->snsConnection;
	}
	
	/**
	 * creates an SNS endpoint
	 * @param string $deviceToken the device Token
	 * @param int $deviceType the device type
	 */
	public function createSNSEndpoint($deviceToken, $deviceType){
		
		$client = $this->_getSNSConnection();
		
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
		
		if (!isset($this->platformArns[$deviceType])){
			throw new \Exception("No Platform ARN for device type");
		}
		
		
		$result = $client->createPlatformEndpoint(
			array(
					'PlatformApplicationArn' => $this->platformArns[$deviceType],
					'Token' => $deviceToken
			));
		
		return $result['EndpointArn'];
	}
	
	
	
	/**
	 * sets an SNS endpoint attriubtes
	 * @param string $endpointArn the endpoint ARN
	 * @param array $attributes the attributes to set
	 */
	public function setSNSEndpointAttributes($endpointArn, $attributes){
		$client = $this->_getSNSConnection();
		
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
		
		$result = $client->SetEndpointAttributes ( array(
				"Attributes" => $attributes,
				'EndpointArn'=>$endpointArn));
		
		return true;
	}
	
	/**
	 * creates a topic
	 * @param string $topicName the topicName 
	 */
	public function createSNSTopic($topicName){
		$client = $this->_getSNSConnection();
		
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
		
		$result = $client->createTopic(array("Name" => $topicName));
		
		return $result['TopicArn'];
	}
	
	/**
	 * creates an sns endpoint subscription
	 * @param string $topicArn the topic arn
	 * @param string $endpointArn the endpoint arn
	 * @return the SNS Endpoint Subscription ARN
	 */
	public function createSNSEndpointSubscription($topicArn, $endpointArn){
		$client = $this->_getSNSConnection();
		
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
		
		$result = $client->subscribe(array(
				"TopicArn" => $topicArn,
				"Protocol" => "application",
				"Endpoint" => $endpointArn
		));
		
		return $result['SubscriptionArn'];
		
	}
	
	/**
	 * get an SNS endpoint attributes
	 * @param $endpointArn the endpoint ARN
	 */
	public function getSNSEndpointAttributes($endpointArn){
	
		$client = $this->_getSNSConnection();
	
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
	
		try{
			$result = $client->GetEndpointAttributes(array('EndpointArn'=>$endpointArn));
		}
		// sns exception occurs if the endpoint subscription doesn't exist
		catch (SnsException $exception){
				
			return false;
		}
		
		if (isset($result['Attributes'])){
			return $result['Attributes'];
		}
	
		return false;
	
	}
	
	/**
	 * get the SNSEndpointSubscription attributes
	 * @param string $subscriptionArn the subscription arn
	 * @return the SNS endpoint Subscriptions attributes
	 */
	public function getSNSEndpointSubscriptionAttributes($subscriptionArn){
		
		$client = $this->_getSNSConnection();
		
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}

		try{
			$result = $client->getSubscriptionAttributes(array(
					'SubscriptionArn' =>  $subscriptionArn,
			));
		}
		// sns exception occurs if the endpoint subscription doesn't exist
		catch (SnsException $exception){
			
			return false;
		}
		if (isset($result['Attributes'])){
			return $result['Attributes'];
		}
		
		return false;	
	}
	
	
	/**
	* get the SNSTopic attributes
	* @param string $topicArn the topic arn
	* @return the SNS topic attributes
	*/
	public function getSNSTopicAttributes($topicArn){
	
		$client = $this->_getSNSConnection();
	
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
	
		try{
			$result = $client->getTopicAttributes(array(
					'TopicArn' =>  $topicArn,
			));
		}
		// sns exception occurs if the endpoint subscription doesn't exist
		catch (SnsException $exception){
				
			return false;
		}
		if (isset($result['Attributes'])){
			return $result['Attributes'];
		}
	
		return false;
	}
	
	/**
	 * deletes the SNSsubscription
	 */
	public function deleteSNSEndpointSubscription($subscriptionArn){
		$client = $this->_getSNSConnection();
		
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
		
		try{
			$result = $client->unsubscribe(array(
					'SubscriptionArn' =>  $subscriptionArn,
			));
		}
		catch (SnsException $exception){
				
		}
		return true;
	}
	
	/**
	 * deleted the sns endpoint
	 */
	public function deleteSNSEndpoint($endpointArn){
		$client = $this->_getSNSConnection();
		
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
		
		try{
			$result = $client->deleteEndpoint(array(
					'EndpointArn' =>  $endpointArn,
			));
		}
		catch (SnsException $exception){
		
		}
		return true;
	}
	
	/**
	 * sends a message to everyone subscribed to a topic
	 * @param $topicArn the topic ARN
	 * @param $message the topic message
	 */
	public function sendMessage($topicArn, $message){
		$client = $this->_getSNSConnection();
		
		
		if (is_null($client)){
			throw new \Exception("SNS Client unavailable");
		}
		
		$encodedMsg = json_encode(array("default"=>json_encode($message),
				"APNS" => json_encode(array("aps"=>array("alert"=>$message))),
				"APNS_SANDBOX" => json_encode(array("aps"=>array("alert"=>$message))),
				"GCM" => json_encode(array("data"=>$message))));
		
		$result = $client->publish(array(
				'TopicArn' =>  $topicArn,
				'Message' => $encodedMsg,
				"MessageStructure" => "json"
		));
		return true;
	}
	
	
}