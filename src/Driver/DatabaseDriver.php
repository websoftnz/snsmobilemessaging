<?php

namespace SellShed\PushNotification\Driver;

/**
 * the database driver 
 * @author SellShed
 *
 */
class DatabaseDriver{
	
	/**
	 * Constructs a database driver using the given config
	 * @param Array $config the config to use
	 */
	
	public function __construct($config){
		$this->config = $config;
		
		if (isset($config['PDO'])){
			$this->connection = $config['PDO'];
		}
	}
	
	/**
	 * private method to get the connection. This will lazy create the connection
	 */
	private function _getConnection(){
		
		if (!isset($this->connection)){
			
			$this->connection = new \PDO($this->config["CONNECTION"],
					$this->config["USERNAME"],
					$this->config["PASSWORD"]);
			
			// TODO should this be removed in production?
			$this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		
		}
		
		return $this->connection;
	}
	
	/**
	 * returns a row from the database if available using the given table, keyname and key
	 * @param String $table_name the table name
	 * @param String $key_name the keyname
	 * @param Unknown  $key the key
	 * @param int $key_type the key type (user the PDO::PARAM_ to determine type)
	 */
	public function getRow($table_name,$key_name, $key, $key_type){
		$conn = $this->_getConnection();
		
		$stmt = $conn->prepare('SELECT * FROM `'.$table_name.'` WHERE `'.$key_name.'` = :id');
		$stmt->bindParam(':id', $key, $key_type);
		
		if (!$stmt->execute()){
			return false;
		}
		
		// fetch row
		return $stmt->fetch();
	}


	/**
	 * returns a row from the database if available using the given table
	 * @param String $table_name the table name
	 */
	public function getAll($table_name){
		$conn = $this->_getConnection();
		
		$stmt = $conn->prepare('SELECT * FROM `'.$table_name.'`');
		
		if (!$stmt->execute()){
			return false;
		}
		
		// fetch row
		return $stmt->fetch();
	}


	/**
	 * updates a row of data in the database
	 * @param String $table_name the table name
	 * @param String $key_name the key name
	 * @param Unknown  $key the key
	 * @param int $key_type the key type (user the PDO::PARAM_ to determine type)
	 * @param Array $column_data an array of data to update. each item in the array must contain an array of Col, type, value. 
	 */
	public function updateRow($table_name, $key_name, $key, $key_type, $column_data){
		
		// get database connection
		$conn = $this->_getConnection();
		
		// create query based on columns given
		$updatevalues= array();
		$count = 0;
		foreach ($column_data as $column){
			$updatevalues [$count] = "`".$column['col']."` = ".$params[$count] = ":p".$count;
			$count ++;
		}
		
		$query = "UPDATE `".$table_name."` SET ".implode(",",$updatevalues)."  WHERE `".$key_name."` = :id";
		
		// prepare statement and bind parameters
		$stmt = $conn->prepare($query);
		
		$count = 0;
		foreach ($column_data as $column){
			$stmt->bindParam($params[$count], $column['value'], $column['type']);
			$count ++;
		}
		
		// bind key
		$stmt->bindParam(':id', $key, $key_type);
		
		// execute statement
		if (!$stmt->execute()){
			return false;
		}
		
		return $stmt->rowCount();
	}
	
	
	/**
	 * updates a row of data in the database
	 * @param String $table_name the table name
	 * @param Array $column_data an array of data to update. each item in the array must contain an array of Col, type, value.
	 */
	public function insertRow($table_name, $column_data){

		// get database connection
		$conn = $this->_getConnection();
		
		// create query based on columns given
		$col_names = array();
		$params = array();
		$count = 0;
		foreach ($column_data as $column){
			$col_names [$count] = "`".$column['col']."`";
			$params[$count] = ":p".$count;
			$count ++;
		}
		
		$query = "INSERT INTO `".$table_name."` (".implode(",",$col_names).") VALUES (".implode(",",$params).")";
		
		// prepare statement and bind parameters
		$stmt = $conn->prepare($query);
		
		$count = 0;
		foreach ($column_data as $column){
			$stmt->bindParam($params[$count], $column['value'], $column['type']);
			$count ++;
		}
		
		// execute statement
		if (!$stmt->execute()){
			return false;
		}
	
		return $conn->lastInsertId();
	}	
	
	/**
	 * Removes a row from the database
	 * @param string $table_name
	 * @param string $key_name
	 * @param string $key
	 * @param string $key_type
	 */
	public function removeRow($table_name, $key_name, $key, $key_type){
		
		$conn = $this->_getConnection();
		
		$stmt = $conn->prepare('DELETE FROM `'.$table_name.'` WHERE `'.$key_name.'` = :id');
		$stmt->bindParam(':id', $key, $key_type);
		
		if (!$stmt->execute()){
			return false;
		} else {
			return true;
		}
	}
	
    /**
	* function to return a database connection to a DAO object
	*/
	public function getDatabaseConnection(){
		return $this->_getConnection();
	}
}