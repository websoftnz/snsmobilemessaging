<?php

namespace SellShed\PushNotification\Driver;

/**
 * This driver controller is used for dependency injecting. All drivers are returned from this class. 
 * A driver controller should be passed into each Logic, controller and DAO object.
 * @author Sell Shed
 */
class DriverController{
	
	// instances of all drivers
	private $instances = array();
	
	private $daos = array();
	
	/**
	 * Constructor
	 * @param String $configdir the directory where all the config is held
	 */
	public function __construct($config){
		
		$this->config = $config;
	}
	
	/**
	 * loads the config file. Config files should just be an array.
	 * @param String $filename The full path name of the file
	 */
	private function loadFile($filename){
		
		return include $filename;
		
	}
	
	/**
	 * return the config file. The given name will relate to a php file found in the config directory
	 * @param String $name the name of the config to return
	 */
	public function getConfig($name){
		if (!isset($this->config[$name])){
			throw new \Exception("Push Notification Error, Config ".$name." not provided in passed in config array");
		}
		return $this->config[$name];
	}


	/**
	 * Used to get a Data Access Object
	 * @param String $name the name of the data access object to get
	 * @return the Data Access Object
	 */
	public function getDAO($name){
	
		if (!isset($this->daos[$name])){
			$classname = "\\SellShed\\PushNotification\\DAO\\".$name."DAO";
			$this->daos[$name] = $classname::instance($this); 
		}
		
		return $this->daos[$name];
	}

	
	/**
	 * get a Database driver with the given name. 
	 * @param string $name If provided this is a key used to look up the config in the database config file
	 * @throws \Exception If no config could be found
	 */
	public function getDatabase($name = "default"){
		
		$key= "DATABASE_".$name;
		
		if (!isset($this->instances[$key])){
			
			$config = $this->getConfig("database");
			
			if (!isset($config[$name])){
				throw new \Exception("No Config found for database ".$name);
			}
			
			$this->instances[$key] = new DatabaseDriver($config[$name]);
			
		}
		
		return $this->instances[$key];
	}


	/**
 	 * Runtime Data Driver
 	 * @param string $name
 	 * @throws \Exception
 	 */
	public function getAmazonSns($name = "default"){
	
		$key = "AMAZON_SNS_" . $name;

		if(!isset($this->instances[$key])){
	
			// use cache data
			$config = $this->getConfig("amazonsns");
			
			if(!isset($config[$name])){
				throw new \Exception("No config found for runtime data " . $name);
			}

			if(isset($config[$name]['driver'])){
				return $this->instances[$key] = $config[$name]['driver'];
				
			}
			return $this->instances[$key] = new AmazonSNSDriver($config[$name]);
		}
	
		return $this->instances[$key];
	
	}	
	
}