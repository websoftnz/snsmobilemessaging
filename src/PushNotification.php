<?php

namespace SellShed\PushNotification;
/**
 * The User Logic Class
 * @author SellShed
 *
 */
class PushNotification {

	/**
	 * IOS DeviceType
	 */
	const DEVICE_TYPE_IOS = 1;
	
	/**
	 * Android DeviceType
	 */
	const DEVICE_TYPE_ANDROID = 2;
	
	/**
	 * the only instance of this class
	 */
	private static $instance;
	
	/**
	 * static method to get the instance of this class
	 * @param \SellShed\Driver\DriverController $drivers the driver controller
	 */
	public static function instance($config){
		if (!isset(self::$instance) || self::$instance ==false ){
			self::$instance = new PushNotification($config);
		}
		return self::$instance ;
	}
	
	static function clearInstance(){
		if (isset(self::$instance)){
			self::$instance = false;
		}
	}
	
	/**
	 * private constructor
	 * @param array $config the config array
	 */
	private function __construct($config){
		$this->driverController = new \SellShed\PushNotification\Driver\DriverController($config);
	}
	
	
	/**
	 * internal method to unlink a device to any linked topics topics
	 * @param array $mobileEndpoint the mobile endpoint
	 */
	private function unlinkDeviceTopics($mobileEndpoint){
		
		// get all endpoint subscriptions
		$SNSMobileEndpointSubscriptionDAO = $this->driverController->getDAO("SNSMobileEndpointSubscription");
		$endpointSubs = $SNSMobileEndpointSubscriptionDAO
			->getAllMobileEndpointSubscriptions($mobileEndpoint['sns_mobile_endpoint_id']);

		$pushNotificationDriver = $this->driverController->getAmazonSNS();
		
		// iterate through them and unsubscribe them
		foreach($endpointSubs as $endpointSub){
			// remove all topics for device in sns
			$pushNotificationDriver->deleteSNSEndpointSubscription($endpointSub['topic_subscription_arn']);
		}
		
		// remove all from Database
		$SNSMobileEndpointSubscriptionDAO->removeFromMobileEndpoint($mobileEndpoint['sns_mobile_endpoint_id']);
	}
	
// 	public function dumpDeviceTable(){
// 		$mobileEndpointDAO = $this->driverController->getDAO("SNSMobileEndpoint");
		
// 		$mobileEndpointDAO->dumpTable();
// 	}
	
	/**
	 * Links a user to a device
	 * @param string $pushToken the device token
	 * @param int $userId the user to identifier
	 */
	public function linkUser($pushToken, $userId){
		$mobileEndpointDAO = $this->driverController->getDAO("SNSMobileEndpoint");
	
		// get the mobile endpoint
		$mobileEndpoint = $mobileEndpointDAO->getFromKey($pushToken);
	
		// check mobile endpoint exists
		if ($mobileEndpoint === false){
			return array(false,"Push Token was not found in the database");
		}
	
		// check not linked already
		if ($mobileEndpoint['fk_user_id'] == $userId){
			return array(true, "Already Linked");
		}
	
		// if linked to another user then unlink first
		if ($mobileEndpoint['fk_user_id'] > 0){
			$this->unlinkDeviceTopics($mobileEndpoint);
		}
	
		// link user to device in database
		$mobileEndpointDAO->setUser($pushToken, $userId);
	
		// ensure device is subscribed to all user topics
		$this->ensureTopicSubscriptions($userId, 
				$mobileEndpoint['sns_mobile_endpoint_endpoint_arn'], 
				$mobileEndpoint['sns_mobile_endpoint_id']);
		

		
		return array(true, "Link Success");
		
	}
	
	/**
	 * unlinks the user from the device
	 * @param $pushToken unlink user
	 */
	public function unlinkUser($pushToken){
	
		$mobileEndpointDAO = $this->driverController->getDAO("SNSMobileEndpoint");
	
		// get the mobile endpoint
		$mobileEndpoint = $mobileEndpointDAO->getFromKey($pushToken);
	
		// check mobile endpoint exists
		if ($mobileEndpoint === false){
			return array(false,"Push Token was not found in the database");
		}
	
		if ($mobileEndpoint['fk_user_id'] > 0){
	
			// remove all topics for device in sns
			$this->unlinkDeviceTopics($mobileEndpoint);
	
			// remove user from device in database
			$mobileEndpointDAO->setUser($pushToken, 0);
	
			return array(true, false);
	
		}
		else
		{
			return array(false,"Push Token was not linked to a user");
		}
	
	
	}
	
	/**
	 * method used to create the SNS endpoint
	 * @param String $pushToken
	 * @param String $deviceType
	 * @throws \Exception
	 */
	private function createSNSEndpoint($pushNotificationDriver, $pushToken, $deviceType){
		$endpointArn = $pushNotificationDriver->createSNSEndpoint($pushToken, $deviceType);
		if ($endpointArn  === false){
		
			throw new \Exception("No Endpoint Arn Provided");
		}
			
		
		return $endpointArn;
	}
	
	/**
	 * method used to create an SNS endpoint subscription
	 * @param string $topicarn
	 * @param string $endpointarn
	 */
	private function createSNSEndpointSubscription($pushNotificationDriver, $topicarn, $endpointarn){
		$endpointArn = $pushNotificationDriver->createSNSEndpointSubscription($topicarn, $endpointarn);
		if ($endpointArn  === false){
			
			return false;
		}
			
	
		return $endpointArn;
	}
	
	
	/**
	 * Make sure the endpoint has topic subscriptions all setup
	 * @param $userId the user identifier
	 * @param $endpointArn the endpoint arn (required to create the subscription)
	 * @param $endpointId the endpoint id (required to store the subsciption in the DB)
	 */
	private function ensureTopicSubscriptions($userId, $endpointArn, $endpointId){
		
		// find user subscriptions and add them all
		$SNSTopicSubscriptionsDAO = $this->driverController->getDAO("SNSTopicSubscription");
		
		$topics = $SNSTopicSubscriptionsDAO->getTopicSubscriptions($userId);
		
		$pushNotificationDriver = $this->driverController->getAmazonSNS();
		
		// Iterate through each Topic subscription
		foreach ($topics as $topic){
			
			// check whether an existing subscription for this endpoint id and user id exists
			$this->ensureEndpointIsSubscribed($pushNotificationDriver, $topic['sns_topic_arn'], 
					$topic['sns_topic_id'], $endpointArn, $endpointId);
		}
		
	}
	
	/**
	 * Ensure device registered
	 * @param string $pushToken the push token
	 * @param int $deviceTyoe the device type
	 * @param int $userId the user id (optional)
	 */
	public function ensureDeviceIsRegistered($pushToken, $deviceType, $userId = false){
	
		// check device type
		if($deviceType !== self::DEVICE_TYPE_ANDROID && $deviceType !== self::DEVICE_TYPE_IOS ){
			throw new \Exception("Invalid device type provided must be either 1 or 2 ");
		}
		
		$mobileEndpointDAO = $this->driverController->getDAO("SNSMobileEndpoint");
	
		// get the mobile endpoint
		$mobileEndpoint = $mobileEndpointDAO->getFromKey($pushToken);
	
		$pushNotificationDriver = $this->driverController->getAmazonSNS();

		// 		if the platform endpoint ARN is not stored this is a first-time registration
		if ($mobileEndpoint  === false){
		
			$endpointArn = $this->createSNSEndpoint($pushNotificationDriver, $pushToken, $deviceType);
			
			$MobileEndpointId = $mobileEndpointDAO->addEndpoint($pushToken, $deviceType, $userId, $endpointArn);
	
			// ensure endpoint is subscribed to topics
			if ($userId!==false){
				$this->ensureTopicSubscriptions($userId, $endpointArn, $MobileEndpointId);
			}
		}
		else{
			// check userid is the same
			$resubscribe = false;
			if ($mobileEndpoint['fk_user_id']!=$userId){
				// we need to remove all subscrscriptions as user id has changed
				
				if ($mobileEndpoint['fk_user_id']!=false){
					$this->unlinkDeviceTopics($mobileEndpoint);
				}
				
				// link user to device in database
				$mobileEndpointDAO->setUser($pushToken, $userId);
				
				$resubscribe = ($userId!==false);
			}
			
			$storedEndpointArn = $mobileEndpoint['sns_mobile_endpoint_endpoint_arn'];

			$attributes = $pushNotificationDriver->getSNSEndpointAttributes($storedEndpointArn);
			
			if ($attributes == false){

					// Have to assume the arn no longer exists	so create a new one and update the row
				$endpointArn = $this->createSNSEndpoint($pushNotificationDriver, $pushToken, $deviceType);
				
				// update row with new endpoint
				$mobileEndpointDAO->updateArn($mobileEndpoint['sns_mobile_endpoint_id'],$endpointArn);
				
				// ensure endpoint is subscribed to topics
				if ($userId!==false){
					$this->ensureTopicSubscriptions($userId, $endpointArn, $mobileEndpoint['sns_mobile_endpoint_id']);
				}
				
			}
			else {
				// check attributes are correct
				if (($attributes['Enabled']!="true") || ($attributes['Token'] != $pushToken)){

					// update attributes
					$attributes['Enabled'] = "true";
					$attributes['Token'] = $pushToken;
					
					$pushNotificationDriver = $pushNotificationDriver->setSNSEndpointAttributes(
							$storedEndpointArn, $attributes);
				}
				
				if ($resubscribe){
					$this->ensureTopicSubscriptions($userId, $storedEndpointArn, $mobileEndpoint['sns_mobile_endpoint_id']);
				}
			}
			
		}
	}


	/**
	 * unregister device
	 */
	public function unregisterDevice($pushToken){

		$mobileEndpointDAO = $this->driverController->getDAO("SNSMobileEndpoint");

		// remove device from databse
		$mobileEndpoint = $mobileEndpointDAO->getFromKey($pushToken);

		if ($mobileEndpoint === false){
			
			return array(false, "No mobile endpoint");
		}	
		
		// remove all Subscriptions for device
		$this->unlinkDeviceTopics($mobileEndpoint);
		
		// remove device from sns
		$pushNotificationDriver = $this->driverController->getAmazonSNS();
		$pushNotificationDriver->deleteSNSEndpoint($mobileEndpoint['sns_mobile_endpoint_endpoint_arn']);
		
		// delete from database
		$mobileEndpointDAO->deleteFromKey($pushToken);

		return array(true, false);
		
	}

	/**
	 * unregister user
	 */
	public function unregisterUser($userId){

		// remove all users topic subscriptions from DB
		$SNSTopicSubscriptions = $this->driverController->getDAO("SNSTopicSubscription");
		$SNSTopicSubscriptions->removeAllUserSubscriptions($userId);

		$pushNotificationDriver = $this->driverController->getAmazonSNS();
		
		// remove all device subscriptions from SNS
		$SNSMobileEndpointSubs = $this->driverController->getDAO("SNSMobileEndpointSubscription");
		
		// get all endpoint subscriptions
		$subscriptions = $SNSMobileEndpointSubs->getAllMobileEndpointSubscriptionsFromUser($userId);


		// go through each SNS endpoint subscription
		foreach ($subscriptions  as $sub){
			
			// remove all topics for device in sns
			$pushNotificationDriver->deleteSNSEndpointSubscription($sub['topic_subscription_arn']);
		}
		
		// remove from DB
		$SNSMobileEndpointSubs->removeAllUserSubscriptions($userId);
		
		// find all devices for user for each device remove user from device
		$mobileEndpointDAO = $this->driverController->getDAO("SNSMobileEndpoint");
		
		// remove device from database
		$mobileEndpointDAO->removeUser($userId);
		
		// We currently aren't removing the MobileEndpoints in case they end up being linked to another user 
	}

	/**
	 * ensures an endpoint is subscribed to a topic
	 */
	private function ensureEndpointIsSubscribed($pushNotificationDriver, $topicArn, $topicid, $endpointArn, $endpointId, $forceReRegister = false){
		
		$SNSMobileEndpointSubscriptionDAO = $this->driverController->getDAO("SNSMobileEndpointSubscription");
		
		$subscription =
			$SNSMobileEndpointSubscriptionDAO->getSubscriptionArn($topicid,
					$endpointId);
			
		// there is no subscription so then create one
		if ($subscription == false){
		
			$subscriptionArn = $this->createSNSEndpointSubscription(
					$pushNotificationDriver, $topicArn, $endpointArn);
		
			if ($subscriptionArn !== false){
				// add endpoint arn to database
				$SNSMobileEndpointSubscriptionDAO->addMobileEndpoint(
						$topicid,
						$endpointId,
						$subscriptionArn);
			}
		}
		else
		{
			
			// check the subscription arn is correct
			$attributes = $pushNotificationDriver->getSNSEndpointSubscriptionAttributes(
					$subscription['topic_subscription_arn']);
		
			// check topic arn and remove if not correct
			if ($forceReRegister || ($attributes !== false && $attributes['TopicArn'] != $topicArn)){
		
				// remove existing subscription
				$pushNotificationDriver->deleteSNSEndpointSubscription($subscription['topic_subscription_arn']);
					
				// make sure a new subscription is created
				$attributes = false;
			}
		
			// if the given arn is not found then we need to create a new arn and update the DB with the new arn address
			if ($attributes === false){
				$subscriptionArn = $this->createSNSEndpointSubscription(
						$pushNotificationDriver,
						$topicArn,
						$endpointArn);
					
				if ($subscriptionArn !== false){
					// Update the DB with the correct ARN
					$SNSMobileEndpointSubscriptionDAO->updateSubscriptionArn(
							$subscription['sns_topic_mobile_subscription_id'],
							$subscriptionArn);
				}
			}
		
		}
	}

	
	/**
	 * subscibes a user to a topic name
	 * @param int $userId the user identifier
	 * @param string $topicName the topic Name
	 */
	public function subscribeTopic($userId, $topicName){
		
		$SNSTopicDAO = $this->driverController->getDAO("SNSTopic");
		// check topic exists in DB
		$topic = $SNSTopicDAO->getTopic($topicName);

		$topicArn = false;
		
		// if not create one
		if ($topic === false){
			$topicid = $SNSTopicDAO->createTopic($topicName);
		}
		else{
			
			$topicid = $topic['sns_topic_id'];
			// check topic arn is valid (if not we will have to create a new ARN
			if (isset($topic['sns_topic_arn']) && $topic['sns_topic_arn'] != ""){
				$topicArn = $topic['sns_topic_arn'];
			}
			
		}
		
		// add user to topic in DB
		$SNSTopicSubscriptionsDAO = $this->driverController->getDAO("SNSTopicSubscription");

		// check whether subscription exists already
		if (!$SNSTopicSubscriptionsDAO->checkUserSubscriptions($topicid, $userId))
		{
			$SNSTopicSubscriptionsDAO->subscribeUserToTopic($topicid, $userId);
		}
		
		// if the topic arn doesn't exist in the datbase then we need to create an ARN on SNS
		$pushNotificationDriver = $this->driverController->getAmazonSNS();
		
		$forceReRegister = false;
		
		if ($topicArn == false){
			$topicArn = $pushNotificationDriver->createSNSTopic($topicName);
			
			// update Topic in DB
			$SNSTopicDAO->linkARNtoTopic($topicid,$topicArn);
			
		}
		else
		{
			// check topic arn still exists
			$attributes = $pushNotificationDriver->getSNSTopicAttributes(
					$topicArn);
			
			// it doesn't so recreate one
			if ($attributes == false){
				$topicArn = $pushNotificationDriver->createSNSTopic($topicName);
					
				// update Topic in DB
				$SNSTopicDAO->linkARNtoTopic($topicid,$topicArn);
				
				$forceReRegister = true;

			}
		}
		
		// get all user end points
		$SNSMobileEndpointDAO = $this->driverController->getDAO("SNSMobileEndpoint");
		
		$mobileEndpoints = $SNSMobileEndpointDAO->getUserEndpoints($userId);

		// iterate through all endpoints and ensure they are subscribed to this topic
		foreach ($mobileEndpoints as $mobileEndpoint){

			$this->ensureEndpointIsSubscribed(
					$pushNotificationDriver, $topicArn, $topicid, 
					$mobileEndpoint['sns_mobile_endpoint_endpoint_arn'],
					$mobileEndpoint['sns_mobile_endpoint_id'], $forceReRegister);
		}
	}

	/**
	 * unsubscribe user from topic
	 */
	public function unsubscribeTopic($userid, $topicName){

		// get Topic Arn from DB
		$SNSTopicDAO = $this->driverController->getDAO("SNSTopic");
		$topic = $SNSTopicDAO->getTopic($topicName);
		
		// only continue if topic was found
		if ($topic === false || !isset($topic['sns_topic_arn']) || $topic['sns_topic_arn'] == ""){
			
			return false;
		}

		$topicArn = $topic['sns_topic_arn'];
		
		// remove user subscriptions
		$SNSTopicSubscriptionsDAO = $this->driverController->getDAO("SNSTopicSubscription");
		
		$SNSTopicSubscriptionsDAO->unsubscribeUserFromTopic($topic['sns_topic_id'], $userid);
		
		// find all endpoint 
		$SNSMobileEndpointSubsDAO = $this->driverController->getDAO("SNSMobileEndpointSubscription");
		$endpointSubs = $SNSMobileEndpointSubsDAO->getAllMobileEndpointSubscriptionsForUserAndTopic($userid, $topic['sns_topic_id']);
		
		$pushNotificationDriver = $this->driverController->getAmazonSNS();
		
		// iterate through them and unsubscribe them
		foreach($endpointSubs as $endpointSub){
			// remove all topics for device in sns
			$pushNotificationDriver->deleteSNSEndpointSubscription($endpointSub['topic_subscription_arn']);
		}
		
		// remove all from Database
		$SNSMobileEndpointSubsDAO->removeFromTopicAndUser($userid, $topic['sns_topic_id']);
		
		return true;
	}
	
	/**
	 * sends a message to a topic
	 * @param string $topicName the topic name to send the message to
	 * @param string $message the message to send
	 */
	public function sendTopicMessage($topicName, $message){
		// get Topic Arn from DB
		$SNSTopicDAO = $this->driverController->getDAO("SNSTopic");
		$topic = $SNSTopicDAO->getTopic($topicName);

		// only continue if topic was found
		if ($topic === false || !isset($topic['sns_topic_arn']) || $topic['sns_topic_arn'] == ""){
			return false;
		}
		
		$pushNotificationDriver = $this->driverController->getAmazonSNS();
		$pushNotificationDriver->sendMessage($topic['sns_topic_arn'],$message);
		
		return true;
	}
}