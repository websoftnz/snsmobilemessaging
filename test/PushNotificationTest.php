<?php
require_once __DIR__ . '/../vendor/autoload.php';
class PushNotificationTest extends PHPUnit_Extensions_Database_TestCase {
	private $conn = null;
	
	static $pdo = false;

	// setup database a construct tables
	public function __construct()
	{
		echo "con --";
		
		if (self::$pdo == false){
			self::$pdo = new PDO('sqlite::memory:');
			self::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}
		
	}
	
	/**
     * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    public function getConnection()
    {
        //$pdo = new PDO('sqlite::memory:');
        
    	if ($this->conn === null) {
    		$this->conn = $this->createDefaultDBConnection(self::$pdo , ':memory:');
    	}
    	
    	return $this->conn;
    	
    }
	
    
    /**
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    public function getDataSet()
    {
    	return $this->createXMLDataSet(dirname(__FILE__).'/test_db.xml');
    }
    
    protected function getSetUpOperation(){
    	
    	$stmt = self::$pdo->prepare("CREATE TABLE `sns_mobile_endpoint` (
			`sns_mobile_endpoint_id` INTEGER PRIMARY KEY AUTOINCREMENT ,
			`sns_mobile_endpoint_push_token`  TEXT NULL,
			`fk_user_id`  BIGINT(20) DEFAULT NULL,
			`sns_mobile_endpoint_endpoint_arn` VARCHAR(255) NULL DEFAULT '',
			`sns_mobile_endpoint_device_type` integer,
			`sns_mobile_endpoint_created_date`  DATETIME NULL DEFAULT NULL,
			`sns_mobile_endpoint_lastupdated_date`  DATETIME NULL DEFAULT NULL);");
    	$stmt->execute();
    	
    	$stmt = self::$pdo->prepare("CREATE TABLE `sns_topic` (
			`sns_topic_id` INTEGER PRIMARY KEY AUTOINCREMENT ,
			`sns_topic_name`  TEXT NULL,
			`sns_topic_arn` VARCHAR(255) NULL DEFAULT '',
			`sns_topic_created_date`  DATETIME NULL DEFAULT NULL,
			`sns_topic_lastupdated_date`  DATETIME NULL DEFAULT NULL);");
    	$stmt->execute();
    	
    	$stmt = self::$pdo->prepare("CREATE TABLE `sns_topic_subscription` (
			`sns_topic_subscription_id` INTEGER PRIMARY KEY AUTOINCREMENT ,
			`fk_user_id`  BIGINT(20) DEFAULT NULL,
			`fk_sns_topic_id`  BIGINT(20) DEFAULT NULL);");
    	
    	$stmt->execute();
    	
    	$stmt = self::$pdo->prepare("CREATE TABLE `sns_topic_mobile_subscription` (
			`sns_topic_mobile_subscription_id` INTEGER PRIMARY KEY AUTOINCREMENT ,
			`fk_sns_mobile_endpoint_id`  BIGINT(20) DEFAULT NULL,
			`fk_sns_topic_id`  BIGINT(20) DEFAULT NULL,
    		`topic_subscription_arn`  VARCHAR(255) NULL DEFAULT '');");
    	 
    	$stmt->execute();
    	
    	return parent::getSetUpOperation();
    }
    
	protected function getTearDownOperation()
	{
		$stmt = self::$pdo->prepare("DROP TABLE sns_mobile_endpoint");
		$stmt->execute();
		$stmt = self::$pdo->prepare("DROP TABLE sns_topic");
		$stmt->execute();
		$stmt = self::$pdo->prepare("DROP TABLE sns_topic_subscription");
		$stmt->execute();
		$stmt = self::$pdo->prepare("DROP TABLE sns_topic_mobile_subscription");
		$stmt->execute();
		
		return parent::getTearDownOperation();
	}
    
	protected function getPushNotificationObject(){
		
		// create Mock Push Notification Driver
		$this->mockSNSDriver = $this->getMockBuilder ( '\SellShed\PushNotification\Driver\AmazonSNSDriver' )
			->disableOriginalConstructor()->getMock ();
		
		\SellShed\PushNotification\PushNotification::clearInstance();
		return \SellShed\PushNotification\PushNotification::instance(array(
				"database"=>array("default"=>array("PDO"=>self::$pdo )),
				"amazonsns"=>array("default"=>array("driver"=>$this->mockSNSDriver))
		));
		
	}
	
    /**
     * the the ensure device is registered method
     */
    public function testEnsureDeviceIsRegisteredInvalidDevice(){
    	$pushNotification = $this->getPushNotificationObject();

    	$this->expectException(Exception::class);

    	// 5 is an invalid device type
    	$pushNotification->ensureDeviceIsRegistered("TestToken", 5, 23);
    		
    }

    public function testEnsureDeviceIsRegisteredSuccess(){
    
    	$pushNotification = $this->getPushNotificationObject();
    	
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSEndpoint')->willReturn("TestEndpointArn")->with("TestToken",
    			\SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS);
    	 
    	
    	// add user device
    	$pushNotification->ensureDeviceIsRegistered("TestToken", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS, 23);
    	 
    	
    }
    
    public function testEnsureDeviceIsRegisteredTheRest(){

    	// register a user with an endpoint that a user already exists on
    	$pushNotification = $this->getPushNotificationObject();

    	$this->mockSNSDriver->expects($this->once())
    	->method('createSNSEndpoint')->willReturn("TestEndpointArn")->with("TestToken",
    			\SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS);
    
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSTopic')->willReturn("TestTopicArn")->with("TopicName1");
    	
    	// and will then expect a call to create the sub endpoint
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSEndpointSubscription')->willReturn("TestSubArn")->with("TestTopicArn",
    			"TestEndpointArn");
    	
    	$pushNotification->subscribeTopic(23, "TopicName1");
    	
        // add device with first user
    	$pushNotification->ensureDeviceIsRegistered("TestToken", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS, 23);

    	$this->mockSNSDriver->__phpunit_verify();
    	
    	
    	// and will then expect a call to create the sub endpoint
    	$this->mockSNSDriver->expects($this->once())
    	->method('deleteSNSEndpointSubscription')->willReturn(true)->with(
    			"TestSubArn");
    	
    	// move to a second user
    	$pushNotification->ensureDeviceIsRegistered("TestToken", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS, 24);
    	 
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	
    	
    }
    
    /**
     * check link to user
     */

    public function testLinkUserToDevice(){
    
    	$pushNotification = $this->getPushNotificationObject();
    	 

    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSEndpoint')->willReturn("TestEndpointArn")->with("TestToken",
    			\SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID);
    	 
    	// register device
    	$pushNotification->ensureDeviceIsRegistered("TestToken", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID, false);
    	$this->mockSNSDriver->__phpunit_verify();
    	 

    	// check with a rubbish token
    	list($success, $message) = $pushNotification->linkUser("CrapToken", 23);
    	$this->assertFalse($success);
    	
    	
    	// check with a correct token
    	list($success, $message) = $pushNotification->linkUser("TestToken", 23);
    	$this->assertTrue($success);


    	// check linking again to the same user
    	list($success, $message) = $pushNotification->linkUser("TestToken", 23);
    	$this->assertTrue($success);
    	$this->assertEquals("Already Linked", $message);
    	

    	// check unsubscription to topics when a different user is chosen to the same token
    	
    	// this will expect a call to create the topic
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSTopic')->willReturn("TestTopicArn")->with("TestTopic");
    	 
    	// and will then expect a call to create the sub endpoint
    	$this->mockSNSDriver->expects($this->once())
	    	->method('createSNSEndpointSubscription')->willReturn("TestSubArn")->with("TestTopicArn",
	    			"TestEndpointArn");

    	
    	// subscribe last user to topic
    	$pushNotification->subscribeTopic(23,"TestTopic");

    	$this->mockSNSDriver->__phpunit_verify();
    	
    	
    	$this->mockSNSDriver->expects($this->once())
    	->method('createSNSTopic')->willReturn("TestTopicArn2")->with("TestTopic2");
    	 
    	$pushNotification->subscribeTopic(15,"TestTopic2");
    	 
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	
    	// and will then expect a call to remove the sub endpoint
    	// when linking with a different user
    	$this->mockSNSDriver->expects($this->once())
    		->method('deleteSNSEndpointSubscription')->willReturn(true)->with("TestSubArn");
    	$this->mockSNSDriver->expects($this->once())
	    	->method('createSNSEndpointSubscription')->willReturn("TestSubArn2")->with("TestTopicArn2",
	    			"TestEndpointArn");
	    	 
    	
    	list($success, $message) = $pushNotification->linkUser("TestToken", 15);
    	$this->assertTrue($success);
    	
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	
    	
    }
    
    /**
     * test unlink user
     */
    public function testUnlinkUser(){
    	
    	$pushNotification = $this->getPushNotificationObject();
    	
		// test with a rubbish token
		list($success, $message) = $pushNotification->unlinkUser("CrapToken");
		$this->assertFalse($success);
		
		// test with a token that was previously unlinked
		$this->mockSNSDriver->expects($this->once())
		->method('createSNSEndpoint')->willReturn("TestEndpointArn")->with("TestToken",
				\SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID);
		
		$pushNotification->ensureDeviceIsRegistered("TestToken", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID, false);
		
		$this->mockSNSDriver->__phpunit_verify();
		
		list($success, $message) = $pushNotification->unlinkUser("TestToken");
		$this->assertFalse($success);
		
		
		
		// Test with a attached User that is subscribed to some topics
		$pushNotification->linkUser("TestToken", 23);
		
		// this will expect a call to create the topic
		$this->mockSNSDriver->expects($this->once())
			->method('createSNSTopic')->willReturn("TestTopicArn")->with("TestTopic");
		$this->mockSNSDriver->expects($this->once())
			->method('createSNSEndpointSubscription')->willReturn("TestSubArn")->with("TestTopicArn",
				"TestEndpointArn");
		$pushNotification->subscribeTopic(23,"TestTopic");
		$this->mockSNSDriver->__phpunit_verify();
		
		
		$this->mockSNSDriver->expects($this->once())
			->method('createSNSTopic')->willReturn("TestTopicArn2")->with("TestTopic2");
		$this->mockSNSDriver->expects($this->once())
			->method('createSNSEndpointSubscription')->willReturn("TestSubArn2")->with("TestTopicArn2",
				"TestEndpointArn");
		$pushNotification->subscribeTopic(23,"TestTopic2");
		$this->mockSNSDriver->__phpunit_verify();
		
		$this->mockSNSDriver->expects($this->exactly(2))
			->method('deleteSNSEndpointSubscription')->willReturn(true)->with(
					$this->logicalOr("TestSubArn2","TestSubArn"));
		list($success, $message) = $pushNotification->unlinkUser("TestToken");
		$this->assertTrue($success);
		
    }
    
    /**
     * test unregister device
     */
    public function testUnregisterDevice(){
    	
    	$pushNotification = $this->getPushNotificationObject();
    	
    	// check with crap token
    	list($success, $message) = $pushNotification->unregisterDevice("CrapToken");
    	$this->assertFalse($success);

    	// check with registered
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSEndpoint')->willReturn("TestEndpointArn")->with("TestToken",
    				\SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID);
    	$pushNotification->ensureDeviceIsRegistered("TestToken", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID, false);
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	$this->mockSNSDriver->expects($this->once())
    		->method('deleteSNSEndpoint')->willReturn(true)->with("TestEndpointArn");
    	list($success, $message) = $pushNotification->unregisterDevice("TestToken");
    	 
    	$this->assertTrue($success);
    	 
    }
    
    /**
     * test unregister user
     */
    public function testUnregisterUser(){

    	$pushNotification = $this->getPushNotificationObject();

    	// *******
    	// test with nothing known about the user
    	$pushNotification->unregisterUser(49);
    	// nothing should happen
    	
    	
    	
    	// **********
    	// test with 1 user on one device
    	// check with registered
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSEndpoint')->willReturn("TestEndpointArn")->with("TestToken",
    			\SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS);
    	 
    	// register device
    	$pushNotification->ensureDeviceIsRegistered("TestToken", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS, 49);
    	$pushNotification->unregisterUser(49);
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	// to test check that the device is not linked to a user
    	list($success, $message) = $pushNotification->unlinkUser("TestToken");
    	$this->assertFalse($success);
    
    	
    	// **********
    	// register user with 2 devices and subscribe to several topics
    	$this->mockSNSDriver->expects($this->exactly(2))
    		->method('createSNSEndpoint')->will($this->onConsecutiveCalls("TestEndpointArnA", "TestEndpointArnB"))->withConsecutive(
    				array("TestTokenA",\SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS),
    				array("TestTokenB",\SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID));
    	 
    	$pushNotification->ensureDeviceIsRegistered("TestTokenA", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS, 49);
    	$pushNotification->ensureDeviceIsRegistered("TestTokenB", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID, 49);
    	
    	
    	$this->mockSNSDriver->__phpunit_verify();
    	
    	$pushNotification->unregisterUser(49);
    	
    	// to test check that the device is not linked to a user
    	list($success, $message) = $pushNotification->unlinkUser("TestTokenA");
    	$this->assertFalse($success);
    	list($success, $message) = $pushNotification->unlinkUser("TestTokenB");
    	$this->assertFalse($success);
    	
    	
    	// **********
    	// register user with 2 devices and subscribe to several topics

    	$this->mockSNSDriver->expects($this->exactly(2))
    		->method("getSNSEndpointAttributes")->will(
    				$this->onConsecutiveCalls(
    						array("Enabled"=>"true", "Token"=>"TestEndpointArnA"),
    						array("Enabled"=>"true", "Token"=>"TestEndpointArnB")))
    				->withConsecutive(array("TestEndpointArnA"),array("TestEndpointArnB"));
    	
    	$pushNotification->ensureDeviceIsRegistered("TestTokenA", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS, 49);
    	$pushNotification->ensureDeviceIsRegistered("TestTokenB", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID, 49);
    	
    	$this->mockSNSDriver->expects($this->exactly(2))
    	->method('createSNSTopic')->will($this->onConsecutiveCalls("TestTopicArn1","TestTopicArn2"))->withConsecutive(
    			array("TopicName1"),
    			array("TopicName2"));
    	
    	$this->mockSNSDriver->expects($this->exactly(4))
    		->method('createSNSEndpointSubscription')->will($this->onConsecutiveCalls("TestSubArn1","TestSubArn2","TestSubArn3","TestSubArn4"))->withConsecutive(
    				array("TestTopicArn1",$this->logicalOr("TestEndpointArnA","TestEndpointArnB")),
    				array("TestTopicArn1",$this->logicalOr("TestEndpointArnA","TestEndpointArnB")),
    				array("TestTopicArn2",$this->logicalOr("TestEndpointArnA","TestEndpointArnB")),
    				array("TestTopicArn2",$this->logicalOr("TestEndpointArnA","TestEndpointArnB")));
    	
    	
    	$pushNotification->subscribeTopic(49, "TopicName1");
    	$pushNotification->subscribeTopic(49, "TopicName2");
    	$this->mockSNSDriver->__phpunit_verify();
    	
    	// data has been setup 
    	
    	$this->mockSNSDriver->expects($this->exactly(4))
    	 	->method('deleteSNSEndpointSubscription')
    	 		->with($this->logicalOr("TestSubArn1","TestSubArn2","TestSubArn3","TestSubArn4"));
    	// now unregister
    	$pushNotification->unregisterUser(49);
    	 
    }
    
    /**
     * test susbscribe topic
     */
    public function testSubscribeTopic(){


    	$pushNotification = $this->getPushNotificationObject();

    	$this->mockSNSDriver->expects($this->once())
	    	->method('createSNSTopic')
    		->willReturn("TestTopicArn1")
    		->with("TopicName1");

    	// subscribe with no endpoints
    	$pushNotification->subscribeTopic(49, "TopicName1");

    	
    	// second subscribe with no endpoints
    	$this->mockSNSDriver->expects($this->once())
	    	->method('getSNSTopicAttributes')
	    	->willReturn(true)
	    	->with("TestTopicArn1");
    	 
    	$pushNotification->subscribeTopic(50, "TopicName1");
    	 
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	
    	// now create endpoints and if subscriptions were there then they should be automaticall subscribed to

    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSEndpoint')->willReturn("TestEndpointArnA")->with(
    			"TestTokenA",\SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS);
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSEndpointSubscription')->willReturn("TestSubArn1")->with(
    			"TestTopicArn1","TestEndpointArnA");
    			 
    	$pushNotification->ensureDeviceIsRegistered("TestTokenA", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS, 49);
    	 
    	$this->mockSNSDriver->__phpunit_verify();
    	
    	// subscribe with a dead topic
    	$this->mockSNSDriver->expects($this->once())
	    	->method('getSNSTopicAttributes')
	    	->willReturn(false)
	    	->with("TestTopicArn1");
    	$this->mockSNSDriver->expects($this->once())
	    	->method('createSNSTopic')
	    	->willReturn("TestTopicArn2")
	    	->with("TopicName1");
    	
    	$this->mockSNSDriver->expects($this->once())
    	->method('deleteSNSEndpointSubscription')->willReturn(true)->with(
    			"TestSubArn1");
    	
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSEndpointSubscription')->willReturn("TestSubArn1")->with(
    			"TestTopicArn2","TestEndpointArnA");
    	 
    	$pushNotification->subscribeTopic(49, "TopicName1");
    	
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	
    }
    
    /**
     * test the unsubscribe topic method
     */
    public function testUnsubscribeTopic(){

    	$pushNotification = $this->getPushNotificationObject();
    	 
    	// ********
    	// test with invalid topic name
    	$success = $pushNotification->unsubscribeTopic(49, "InvalidTopic");
    	$this->assertFalse($success);
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	
    	// *******
    	// test when a couple of topic are subscribed to
    	
    	$this->mockSNSDriver->expects($this->exactly(2))
    		->method('createSNSEndpoint')->will($this->onConsecutiveCalls("TestEndpointArnA", "TestEndpointArnB"))->withConsecutive(
    				array("TestTokenA",\SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS),
    				array("TestTokenB",\SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID));
    	
    	$pushNotification->ensureDeviceIsRegistered("TestTokenA", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_IOS, 49);
    	$pushNotification->ensureDeviceIsRegistered("TestTokenB", \SellShed\PushNotification\PushNotification::DEVICE_TYPE_ANDROID, 49);
    	 
    	$this->mockSNSDriver->expects($this->exactly(2))
    	->method('createSNSTopic')->will($this->onConsecutiveCalls("TestTopicArn1","TestTopicArn2"))->withConsecutive(
    			array("TopicName1"),
    			array("TopicName2"));
    	 
    	$this->mockSNSDriver->expects($this->exactly(4))
    	->method('createSNSEndpointSubscription')->will($this->onConsecutiveCalls("TestSubArn1","TestSubArn2","TestSubArn3","TestSubArn4"))->withConsecutive(
    			array("TestTopicArn1",$this->logicalOr("TestEndpointArnA","TestEndpointArnB")),
    			array("TestTopicArn1",$this->logicalOr("TestEndpointArnA","TestEndpointArnB")),
    			array("TestTopicArn2",$this->logicalOr("TestEndpointArnA","TestEndpointArnB")),
    			array("TestTopicArn2",$this->logicalOr("TestEndpointArnA","TestEndpointArnB")));
    	$pushNotification->subscribeTopic(49, "TopicName1");
    	$pushNotification->subscribeTopic(49, "TopicName2");
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	
    	// data setup now check the unubscribe to topic
    	
    	$this->mockSNSDriver->expects($this->exactly(2))
    	->method('deleteSNSEndpointSubscription')->with(
    			$this->logicalOr("TestSubArn1","TestSubArn2"));
    	 
    	
    	$pushNotification->unsubscribeTopic(49, "TopicName1");
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	
    }
    
    /**
     * test send message
     */
    public function testSendMessage(){
    	$pushNotification = $this->getPushNotificationObject();
    	
    	// ********
    	// test with invalid topic name
    	$success = $pushNotification->sendTopicMessage("InvalidTopic","Test!");
    	$this->assertFalse($success);
    	
    	
    	// *******
    	// test with a valid topic name
    	$this->mockSNSDriver->expects($this->once())
    		->method('createSNSTopic')->willReturn("TestTopicArn")->with("TopicName");
    	 
    	$pushNotification->subscribeTopic(49, "TopicName");
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    	$this->mockSNSDriver->expects($this->once())
    		->method('sendMessage')->with("TestTopicArn","Test Message!");
    	 
    	$success = $pushNotification->sendTopicMessage("TopicName","Test Message!");
    	 
    	$this->assertTrue($success);
    	$this->mockSNSDriver->__phpunit_verify();
    	 
    }
}